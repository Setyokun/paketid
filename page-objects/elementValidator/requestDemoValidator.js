module.exports = {
  elements: {
    // code dibawah ini adalah element validator
    loadingIcon: 'i.el-icon-loading',
    thankyou: 'h1.requestSuccessTitle'
  },
  commands: [{
    validateFailToRequest() {
      return this
      .assert.elementPresent('@loadingIcon')
      .assert.urlContains('https://demo.mile.app/request-demo', 'Params Value : Request Demo');
    },
    validateThankyou(hiya) {
      return this 
      .assert.urlContains('https://demo.mile.app/request-succes', 'Params Value : Request Success')
      .assert.containsText('@thankyou', hiya, 'Validate Thankyou : Thank you for contacting us!');
    }

  }],
};
