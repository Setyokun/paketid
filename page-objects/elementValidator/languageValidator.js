module.exports = {
  elements: {
    // code dibawah ini adalah element validator
    validateLanguage: '.justify-content-between.language',
  },
  commands: [{
    validateLanguage() {
      return this
        .pause(2000)
        .assert.containsText('@validateLanguage', 'Hubungi Kami', 'Validate Language : Hubungi Kami')
    },
  }],
};
