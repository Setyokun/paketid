const { client } = require('nightwatch-api');

const switchWindowToTab = function (browser) {
  browser.windowHandles(function(result) {
    var handle = result.value[1];
    browser.switchWindow(handle);
  });
};

const switchWindowOriginal = function (browser) {
  browser.windowHandles(function(result) {
    var handle = result.value[0];
    browser.switchWindow(handle);
  });
};

module.exports = {
  elements: {
  },
  commands: [{
    async validateLinkedinPage() {
      switchWindowToTab(client)
      await this
      .assert.urlContains('https://www.linkedin.com/', 'Params Value : Linkedin')
      .pause(2000);
      await this.closeWindow();
      switchWindowOriginal(client);
    },
    async validateFacebookPage() {
      switchWindowToTab(client)
      await this
      .assert.urlContains('https://www.facebook.com/kode.paket', 'Params Value : facebook - paket')
      .pause(2000);
      await this.closeWindow();
      switchWindowOriginal(client);
    },
    async validateInstagramPage() {
      switchWindowToTab(client)
      await this
      .assert.urlContains('https://www.instagram.com/paket.id/', 'Params Value : instagram - paket')
      .pause(2000);
      await this.closeWindow();
      switchWindowOriginal(client);
    },
    async validateTwitterPage() {
      switchWindowToTab(client)
      await this
      .assert.urlContains('https://twitter.com/paket_id', 'Params Value : twitter - paket')
      .pause(2000);
      await this.closeWindow();
      switchWindowOriginal(client);
    },
  }],
};

