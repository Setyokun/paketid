const words = 'Setyokun Company';
module.exports = {
  elements: {
    // code dibawah ini adalah element validator
    validateFail: '.el-card__body div span:nth-child(2)',
  },
  commands: [{
    validateContactUs() {
      return this
        .assert.urlContains('request-demo', 'Params Value : request Demo')
    },
    validateFail() {
      return this
        .pause(2000)
        .assert.containsText('@validateFail', words, 'Validate Fail : Setyokun Company')
    },
  }],
};
