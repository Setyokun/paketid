module.exports = {
  url: 'https://demo.mile.app/landing',
  // code dibawah ini adalah element selector
  elements: {
    linkedinIcon: '.icon-link a:nth-child(1)',
    facebookIcon: '.icon-link a:nth-child(2)',
    instagramIcon: '.icon-link a:nth-child(3)',
    twitterIcon: '.icon-link a:nth-child(4)',
  },

  // pada commands dibawah ini berisi tentang commands-command yang nantinya dipakai di test/steps
  commands: [{
    clickLinkedinIcon() {
      return this
        .moveToElement('@linkedinIcon', 0, 0)
        .click('@linkedinIcon');
    },
    clickFacebookIcon() {
      return this
        .moveToElement('@facebookIcon', 0, 0)
        .click('@facebookIcon');
    },
    clickInstagramIcon() {
      return this
        .moveToElement('@instagramIcon', 0, 0)
        .click('@instagramIcon');
    },
    clickTwitterIcon() {
      return this
        .moveToElement('@twitterIcon', 0, 0)
        .click('@twitterIcon');
    },
  }],
};
