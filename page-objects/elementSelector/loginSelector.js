module.exports = {
  url: 'https://demo.mile.app/landing',
  // code dibawah ini adalah element selector
  elements: {
    login: '.mr-5.nav-item',
    inputOrganization: 'input.el-input__inner',
    buttonLogin: '.button.el-button--primary',
    contactUs: '[href="/request-demo"]',
  },

  // pada commands dibawah ini berisi tentang commands-command yang nantinya dipakai di test/steps
  commands: [{
    clickLogin() {
      return this
        .click('@login');
    },
    inputOrganization(value) {
      return this
        .setValue('@inputOrganization',value);
    },
    clickButtonLogin() {
      return this
        .click('@buttonLogin');
    },
    clickContactUs() {
      return this
        .click('@contactUs');
    },
  }],
};
