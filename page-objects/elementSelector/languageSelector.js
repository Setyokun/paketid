module.exports = {
  url: 'https://demo.mile.app/landing',
  // code dibawah ini adalah element selector
  elements: {
    changeLanguage: '.el-dropdown-link.el-dropdown-selfdefine',
    changeLanguageToIndonesia: 'li:nth-child(2).el-dropdown-menu__item',
  },

  // pada commands dibawah ini berisi tentang commands-command yang nantinya dipakai di test/steps
  commands: [{
    clickChangeLanguage() {
      return this
        .click('@changeLanguage');
    },
    clickChangeLanguageToIndonesia() {
      return this
        .click('@changeLanguageToIndonesia');
    },
  }],
};
