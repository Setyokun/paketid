module.exports = {
  url: 'https://demo.mile.app/landing',
  // code dibawah ini adalah element selector
  elements: {
    requestDemo: 'p.signup',
    inputFullname: '.inputLogin div:nth-child(1) [name="name"]',
    inputEmail: '.inputLogin div:nth-child(1) [name="email"]',
    inputPhoneNumber: '.inputLogin div:nth-child(3) input',
    inputCompanyName: '.inputLogin div:nth-child(4) input',
    buttonRequestDemo: '.button.el-button--primary',
  },

  // pada commands dibawah ini berisi tentang commands-command yang nantinya dipakai di test/steps
  commands: [{
    clickRequestDemo() {
      return this
        .click('@requestDemo');
    },
    async inputDataRequestDemo(fullname, email, phoneNumber, companyName) { 
      await this.setValue('@inputFullname',fullname)
      await this.setValue('@inputEmail',email)
      await this.setValue('@inputPhoneNumber',phoneNumber)
      await this.setValue('@inputCompanyName',companyName);
    },
    clickButtonRequestDemo() {
      return this
        .click('@buttonRequestDemo')
        .pause(2000);
    },
  }],
};
