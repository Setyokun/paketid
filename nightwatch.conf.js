module.exports = {
  // src_folders: ['tests'], // folder yang digunakan untuk acuan test
  page_objects_path: ['page-objects'], // folder yang digunakan untuk page-object di load

  webdriver: {
    start_process: true,
    server_path: 'node_modules/.bin/chromedriver', // path chrome driver
    port: 9515,
  },

  test_settings: {
    default: {
      screenshots: {
        enabled: true,
        path: 'tests_output',
      },
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        chromeOptions: {
          // args: ['--headless'], // --headless : agar ketika script test di run tidak muncul browsernya
          // start-maximezed = buat maximalin page browser,
          // --disable-geolocation = inactive location,
          // --disable-notifications = inactive notification,
          // --disable-popup-blocking = inactive popup
          // --incognito = open in incognito
          // '--disable-geolocation', '--disable-notifications', '--disable-popup-blocking'
          // args: ['start-maximized', '--disable-notifications', '--disable-geolocation', '--disable-popup-blocking'],
          args: ['start-maximized'],
          // args: ['--user-agent=Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 640 XL LTE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10166'],
          // args: ['--user-agent=Mozilla/5.0 (Linux; Android 9; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Mobile Safari/537.36',
          // '--enable-automation',
          // '--window-size=360,650',
          // // '--headless',
          // '--no-sandbox',
          // '--disable-webgl=true',
          // '--disable-3d-apis=true',
        // ],
        },
      },
    },
  },
};
