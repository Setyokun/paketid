Feature: Check Social Media in Footer Feature

Background: open site demo mile app for footer function checking
Given Open site demo mile app for footer function checking


Scenario: Check result when click Linkenin icon 
When Click Linkedin icon in footer
Then Direct to PaketID Linkedin Account

Scenario: Check result when click Facebook icon 
When Click Facebook icon in footer
Then Direct to PaketID Facebook Account

Scenario: Check result when click Instagram icon 
When Click Instagram icon in footer
Then Direct to PaketID Instagram Account

Scenario: Check result when click Twitter icon 
When Click Twitter icon in footer
Then Direct to PaketID Twitter Account
