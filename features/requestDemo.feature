Feature: Check Request Demo Feature 

Background: open site demo mile app for footer function checking
Given I open site demo mile app for check request demo feature

@setyokun
Scenario Outline: Check Result when input fullname, email, phone number, and company name
  When Click Request Demo1
  When Input Fullname "<fullname>" email "<email>" phone number "<phoneNumber>" and company name "<companyName>"
  When Click Button Request Demo1
  Then Content "<result>" must be appear
  Examples:
  | fullname | email | phoneNumber | companyName | result |
  | Setyokun | setyokun@mailinator.com | 081383250055 | Setyokun Company | Thank you for contacting us! |
  | Setyokun | setyokunmailinator | 081383250055 | Setyokun Company | Thank you for contacting us! |
  | Setyokun | setyokun@mailinator.com | abcdefghi | Setyokun Company | Thank you for contacting us! |

  Scenario: Check Result when not input fullname, email, phone number, and company name
  When Click Request Demo2
  When Click Button Request Demo2
  Then Loading icon must be appear and stay in page