const { client } = require('nightwatch-api');
const { Given, Then, When} = require('cucumber');

//load page-object yang dibuar di folder page-object 
const steps = client.page.elementSelector.footerSelector();
const validator = client.page.elementValidator.footerValidator();


Given(/^Open site demo mile app for footer function checking$/, () => {
    return steps.navigate();
});

When(/^Click Linkedin icon in footer$/, () => {
    return steps.clickLinkedinIcon();
});

Then(/^Direct to PaketID Linkedin Account$/, () => {
    return validator.validateLinkedinPage();
});

//===

When(/^Click Facebook icon in footer$/, () => {
    return steps.clickFacebookIcon();
});

Then(/^Direct to PaketID Facebook Account$/, () => {
    return validator.validateFacebookPage();
});

//===

When(/^Click Instagram icon in footer$/, () => {
    return steps.clickInstagramIcon();
});

Then(/^Direct to PaketID Instagram Account$/, () => {
    return validator.validateInstagramPage();
});

//===

When(/^Click Twitter icon in footer$/, () => {
    return steps.clickTwitterIcon();
});

Then(/^Direct to PaketID Twitter Account$/, () => {
    return validator.validateTwitterPage();
});