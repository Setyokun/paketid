const { client } = require('nightwatch-api');
const { Given, Then, When} = require('cucumber');

//load page-object yang dibuar di folder page-object 
const steps = client.page.elementSelector.requestDemoSelector();
const validator = client.page.elementValidator.requestDemoValidator();


Given(/^I open site demo mile app for check request demo feature$/, () => {
    return steps.navigate();
});

When(/^Click Request Demo1$/, () => {
    return steps.clickRequestDemo();
});

When(/^Input Fullname "([^"]*)" email "([^"]*)" phone number "([^"]*)" and company name "([^"]*)"$/, async (fullname, email, phoneNumber, companyName) => {
    return await steps
    .inputDataRequestDemo(fullname, email, phoneNumber, companyName);
});

When(/^Click Button Request Demo1$/, () => {
    return steps.clickButtonRequestDemo();
});

Then(/^Content "([^"]*)" must be appear$/, (value) => {
    return validator.validateThankyou(value);
});

//===

When(/^Click Request Demo2$/, () => {
    return steps.clickRequestDemo();
});

When(/^Click Button Request Demo2$/, () => {
    return steps.clickButtonRequestDemo();
});

Then(/^Loading icon must be appear and stay in page$/, () => {
    return validator.validateFailToRequest();
});

