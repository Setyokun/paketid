const { client } = require('nightwatch-api');
const { Given, Then, When, And} = require('cucumber');

//load page-object yang dibuar di folder page-object 
const steps = client.page.elementSelector.loginSelector();
const validator = client.page.elementValidator.loginValidator();


Given(/^I open site demo mile app1$/, () => {
    return steps.navigate();
});

When(/^Click Login$/, () => {
    return steps.clickLogin();
});

When(/^Input Not Registered Organization Name "([^"]*)"$/, (value) => {
    return steps
    .inputOrganization(value);
});

When(/^Click Button Login$/, () => {
    return steps.clickButtonLogin();
});

Then(/^User can't be logged in and error message must be appear$/, async () => {
    return validator.validateFail();
});

//===========================

Given(/^I open site demo mile app2$/, () => {
    return steps.navigate();
});

When(/^Click Login2$/, () => {
    return steps.clickLogin();
});

When(/^Click Contact us$/, () => {
    return steps.clickContactUs();
});

Then(/^Direct to Request Demo Page$/, async () => {
    return validator.validateContactUs();
});
