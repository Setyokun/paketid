const { client } = require('nightwatch-api');
const { Given, Then, When} = require('cucumber');

//load page-object yang dibuar di folder page-object 
const steps = client.page.elementSelector.languageSelector();
const validator = client.page.elementValidator.languageValidator();


Given(/^Open site demo mile app for check language$/, () => {
    return steps.navigate();
});

When(/^Click English in Right Corner$/, () => {
    return steps.clickChangeLanguage();
});

When(/^Click Indonesia to Change Language in Bahasa$/, () => {
    return steps.clickChangeLanguageToIndonesia();
});

Then(/^Language changed to Bahasa$/, async () => {
    return validator.validateLanguage();
});


